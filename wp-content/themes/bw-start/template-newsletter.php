<?php

/** Template Name: Newsletter
 *  Description: Newsletter Template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();



    // renders page
    Timber::render('template-newsletter.twig', $context);
