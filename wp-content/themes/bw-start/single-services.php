<?php

/** Template Name: Single Service
 *  Description: The template for the Single Service page
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();



    // renders page
    Timber::render('single-service.twig', $context);
