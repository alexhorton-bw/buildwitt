<?php

/** Template Name: Services
 *  Description: Services Template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();

    // gets the most recent posts
    $partners = array(
      'post_type' => 'partners'
      // 'paged' => $paged,
      // 'posts_per_page' => 4
    );

    $context['partners'] = new Timber\PostQuery($partners);


    // renders page
    Timber::render('page-services.twig', $context);

    
