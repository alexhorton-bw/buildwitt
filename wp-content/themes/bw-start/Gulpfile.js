var gulp = require('gulp')
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer')
var rename = require('gulp-rename')

var input = './sass/**/*.sass'
var output = './'

// var autoprefixerOptions = {
//   browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
// }


function style() {
  return (
    gulp
      .src(input)

      .pipe(sass())

      .pipe(autoprefixer())

      .pipe(rename('style.css'))

      .pipe(gulp.dest(output))

  )
}

exports.style = style

function watch(){

    // gulp.watch takes in the location of the files to watch for changes
    // and the name of the function we want to run on change
    gulp.watch(input, style)
}

// Don't forget to expose the task!
exports.watch = watch
