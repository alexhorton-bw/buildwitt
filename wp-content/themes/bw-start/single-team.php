<?php

/** Template Name: Single Team
 *  Description: The template for the Single Team page
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();

    // $team = array(
    //   'post_type' => 'team'
    // );
    //
    // $context['team'] = new Timber\PostQuery($team);

    // renders page
    Timber::render('single-team.twig', $context);
