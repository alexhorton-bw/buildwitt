<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;

// gets the most recent posts
$stories= array(
	'post_type' => 'stories',
	// 'paged' => $paged,
	'posts_per_page' => 5
);

$context['stories'] = new Timber\PostQuery($stories);


// renders page
Timber::render('single-stories.twig', $context);

// if ( post_password_required( $timber_post->ID ) ) {
// 	Timber::render( 'single-password.twig', $context );
// } else {
// 	Timber::render( array( 'single-' . $timber_post->ID . '.twig', 'single-' . $timber_post->post_type . '.twig', 'single-' . $timber_post->slug . '.twig', 'single.twig' ), $context );
// }
