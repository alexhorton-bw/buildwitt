jQuery( document ).ready( function( $ ) {

  // small plugin to detect if something is in the viewport
  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top - 200;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };


  // -------- global logic ------------- //
  $('#burgerClose').hide()
  $('#burgerCloseSticky').hide()

  // slide in mobile menu
  $('#burgerBars').click(function() {
    $('.mobileNav').toggleClass('offScreenLeft')
    $('#burgerBars').fadeToggle(300)
    $('#burgerClose').delay(400).fadeToggle(300)
  })

  $('#burgerClose').click(function() {
    $('.mobileNav').toggleClass('offScreenLeft')
    $('#burgerClose').fadeToggle(300)
    $('#burgerBars').delay(400).fadeToggle(300)
  })

  // slide in mobile menu - sticky nav
  $('#burgerBarsSticky').click(function() {
    $('.mobileNav').toggleClass('offScreenLeft')
    $('#burgerBarsSticky').fadeToggle(300)
    $('#burgerCloseSticky').delay(400).fadeToggle(300)
  })

  $('#burgerCloseSticky').click(function() {
    $('.mobileNav').toggleClass('offScreenLeft')
    $('#burgerCloseSticky').fadeToggle(300)
    $('#burgerBarsSticky').delay(400).fadeToggle(300)
  })

  // sticky nav enter viewport
  let deviceHeight = $(window)[0].innerHeight



  $(window).scroll(function() {
    if ( $(window).scrollTop() > deviceHeight ) {
      $('.stickyMobileNav').removeClass('hideStickyMobileNav')
    } else {
      $('.stickyMobileNav').addClass('hideStickyMobileNav')
    }
  })

  // slides in main menu from side sticky nav
  let stickyNavTrigger = $('#stickyNavTrigger')

  stickyNavTrigger.click(function() {
    $('.fullNav').toggleClass('showFullNav')
  })




  //scroll nav trigger
  $('#navTriggerScroll').click(function() {
    slideParentMenu()
    slideChildMenu()
  })

// slide in menu on scroll
let heroHeight = $('.hero').height()

let heightForScrollNav = heroHeight
// console.log(heightForScrollNav)

if ( $('body').hasClass('home') ) {
  $(window).scroll(function() {
    if ( $(window).scrollTop() > heightForScrollNav ) {
      $('.stickyNav').addClass('showStickyNav')

    } else {
      $('.stickyNav').removeClass('showStickyNav')
    }
  })
} else {
  $('.stickyNav').addClass('showStickyNav')
}





// grid animations! ----------- //

// add classes to all instances of a few elements
$('.greybox').addClass('pre-greybox')
$('.imgBlock').addClass('pre-imgBlock')

if ( $('horizontal').hasClass('pre-horizontal') ) {

} else {
  $('horizontal').addClass('pre-horizontal')
}

if ( $('vertical').hasClass('pre-vertical') ) {

} else {
  $('vertical').addClass('pre-vertical')
}

  // if ALREADY VISIBLE
$('.hero .cross').removeClass('pre-cross')
$('.hero .horizontal').removeClass('pre-horizontal')
$('.hero .vertical').removeClass('pre-vertical')
$('.hero .heroText').removeClass('preHeroText')
$('.hero .imgBlock').removeClass('pre-imgBlock')


$(window).scroll(function(e) {

  $('.cross').each(function() {
    if ( $(this).isInViewport() ) {
      let matchingHorizontal = $(this)[0].previousElementSibling

      let matchingVertical = $(this)[0].nextElementSibling
      $(this).removeClass('pre-cross')
      $(matchingHorizontal).removeClass('pre-horizontal')
      $(matchingVertical).removeClass('pre-vertical')
    }
  })

  // greybox float in
  $('.greybox').each(function() {
    if ( $(this).isInViewport() ) {
      $(this).removeClass('pre-greybox')
    }
  })

  $('.imgBlock').each(function() {
    if ( $(this).isInViewport() ) {
      $(this).removeClass('pre-imgBlock')
    }
  })

  // $('.horizontal').each(function() {
  //   if ( $(this).isInViewport() ) {
  //     $(this).removeClass('pre-horizontal')
  //   }
  // })
  //
  // $('.vertical').each(function() {
  //   if ( $(this).isInViewport() ) {
  //     $(this).removeClass('pre-vertical')
  //   }
  // })

})



// ----- hides top nav on non home pages ------- //

if ($(window).width() > 899) {
  if ( $('body').hasClass('home') ) {
    $('.stationaryNav').addClass('homepage')
  } else {
    $('.stationaryNav').addClass('hidden')
  }
}




if ( $('body').is('.home') ) {

  $(window).scroll(function(e) {

    if ( $('section.people').isInViewport() ) {
      // $('section.people .grid .images').addClass('clearOutHalf')
      $('section.people .grid .textWrap').addClass('textWrapSlideIn')
    }

    if ($('.line.bottomLine').isInViewport()) {
      $('.line.topLine .crossout').addClass('slideInCrossOut')
      $('.line.bottomLine .crossout').addClass('slideInCrossOut')
    }

  })


}


/// GENERAL ANIMATIONS ///

setTimeout(function() {
  $('.slideFromRight').removeClass('slideFromRight')
}, 50)




// ------ CONTACT PAGE ------ //
if ( $('body').is('.page-template-template-contact') ) {
  $('#say-hi-trigger').click(function() {
    // $('.pum-overlay').fadeIn(500)
  })




  $('.accordion .openTab').hide()
  // accordion
  $('.tab').click(function() {
    // $('.accordion .openTab').hide()
    // $(`.tab .cross`).removeClass('activeTab')

    let currentTab = $(this)[0].id.split("-")[1]

    $(`#openTab-${currentTab}`).slideToggle(500)
    $(`#cross-${currentTab}`).toggleClass('activeTab')
  })


  $(window).scroll(function(e) {

    $('.steps .step').each(function() {
      if ( $(this).isInViewport() ) {
        $(this).removeClass('offScreenStep')
      }
    })

    if ( $('.accordion').isInViewport() ) {
      $('.accordion').removeClass('pre-accordion')
    }

  })

}

// ------ CAREERS PAGE ------ //
if ( $('body').is('.page-template-template-careers') ) {

  $(window).scroll(function(e) {

    $('.steps .step').each(function() {
      if ( $(this).isInViewport() ) {
        $(this).removeClass('offScreenStep')
      }
    })


  })

}

// ---- SWIPER ON INDIVIDUAL PAGE ------- //

let teamSlider = new Swiper('.swiper-container', {
  // slidesPerView: 1,
  spaceBetween: 10,
  // loop: true,
  freeMode: true
})


// ----------  TEAM PAGE ------------ //
if ( $('body').is('.page-template-template-team') ) {

  $(window).scroll(function() {
    $('.member').each(function() {
      if ( $(this).isInViewport() ) {
        $(this).removeClass('pushedDown')
      }
    })

    if ( $('.follow-social .feed').isInViewport() ) {
      $('.follow-social .feed').removeClass('pre-feed')
    }

  })



}


// ----------  partner PAGE ------------ //
if ( $('body').is('.page-template-template-partners') ) {


  // removes "All" label from select boxes on partner page
  let initialSelectLabel = $('select option:first-child')
  initialSelectLabel.each(function(i) {
    let arr = $(this)[0].innerHTML.split(' ')
    arr.shift()
    let composedStr = arr.join(' ')

    $(this)[0].innerHTML = composedStr
  })


}

// SINGLE PARTNER PAGE(s)
if ( $('body').is('.single-partners') ) {


  // removes "All" label from select boxes on partner page
  let initialSelectLabel = $('select option:first-child')
  initialSelectLabel.each(function(i) {
    let arr = $(this)[0].innerHTML.split(' ')
    arr.shift()
    let composedStr = arr.join(' ')

    $(this)[0].innerHTML = composedStr
  })

  $('.video').hide()

  $('#play_partner_video').click(function() {
    $('.video').fadeIn(700)
    // $('.arve-video').get(0).play()
  })

  $('#closeVideo').click(function() {
    $('.video').fadeOut(700)
    // $('.arve-video').get(0).pause()
  })

  if ( $('.video:hidden') ) {
    $(document).keyup(function(e) {
      if (e.keyCode === 27) { // esc
        $('.video').fadeOut(700)
        // $('.arve-video').get(0).pause()
      }
    });
  }

  // gallery slider
  let gallerySlider = new Swiper('.gallery-slider', {
    slidesPerView: 1,

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next-gallery',
      prevEl: '.swiper-button-prev-gallery',
    }

  })

  // gallery slider
  let photographySlider = new Swiper('.photography-slider', {
    slidesPerView: 1,

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next-photography',
      prevEl: '.swiper-button-prev-photography',
    }

  })


}

let partnerSlider = new Swiper('.partner-slider', {
  slidesPerView: 'auto',
  spaceBetween: 10,

  // Navigation arrows
  // navigation: {
  //   nextEl: '.swiper-button-next-partner',
  //   prevEl: '.swiper-button-prev-partner',
  // }

})

// ------ Services PAGE ------ //
if ( $('body').is('.page-template-template-services') ) {

  $(window).scroll(function(e) {

    $('.steps .step').each(function() {
      if ( $(this).isInViewport() ) {
        $(this).removeClass('offScreenStep')
      }
    })

  })

}

// ------ About Us PAGE ------ //
if ( $('body').is('.page-template-template-about') ) {
  $('.videoWrap').hide()

  $('#videoTriggerAbout').click(function() {
    $('.videoWrap').fadeIn(700)
    $('.arve-video').get(0).play()
  })

  $('#closeVideo').click(function() {
    $('.videoWrap').fadeOut(700)
    $('.arve-video').get(0).pause()
  })

  if ( $('.videoWrap:hidden') ) {
    $(document).keyup(function(e) {
      if (e.keyCode === 27) { // esc
        $('.videoWrap').fadeOut(700)
        $('.arve-video').get(0).pause()
      }
    });
  }

  $('.valueList .value').each(function() {
    $(this).click(function() {

      // expands to show extra text
      $($(this)[0].firstElementChild.lastElementChild).slideToggle(500)
      $($(this)[0].firstElementChild).toggleClass('selected')
      $($(this)[0].lastElementChild).toggleClass('selected')

    })
  })

}

let aaronSlider = new Swiper('.swiper-container-aaron', {
  slidesPerView: 1,
  // spaceBetween: 10,

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  }

})

// ---------- BLOG PAGE ----------- //
$('.filter .form-wrap').hide()
$('.visibleFilter').click(function() {
  $('.filter .form-wrap').slideToggle(600)
  $('.visibleFilter img').toggleClass('rotate')
})

// Filtering
$('#filter').submit(function(){
    $('.blogpage .pagination').hide()
    $('.filter .form-wrap').slideToggle(600)
    $('.visibleFilter img').toggleClass('rotate')
    $('.all-blogs').fadeOut(300)
    $('.filtered-blogs .singleblog').fadeOut(300)
    var filter = $('#filter')
    $.ajax({
        url:filter.attr('action'),
        data:filter.serialize(), // form data
        type:filter.attr('method'), // POST
        beforeSend:function(xhr){
            // filter.find('button').text('Applying...');
          },
        success:function(data){
            filter.find('button').text('Apply')
            let parsed = JSON.parse(data)

            for (let i = 0; i < parsed.length; i++) {
              let singleBlog =
              `
              <a href="${parsed[i].link}" class="singleblog">
                <div class="opaque"></div>
                <div class="img" style="background-image: url('${parsed[i].thumbnail}')"></div>
                <p class="date">${parsed[i].date}</p> <!-- date -->
                <h2>${parsed[i].title}</h2> <!-- title -->
                <div class="underline"></div>
                <div class="content">${parsed[i].excerpt}</div>
                <div class="greyishbox"></div>
                <div class="readmore">
                  <h3>Read More</h3>
                  <div class="underline"></div>
                </div>
              </a href="${parsed[i].link}">
              `
              $('#filtered-blogs').append(singleBlog)
            }

            // console.log(data[0])
            // $('#response').html(arr)
        }
    });
    return false;
})



// ---------- Stories PAGE ----------- //
$('.filterStories .form-wrap').hide()
$('.visibleFilter').click(function() {
  $('.filterStories .form-wrap').slideToggle(600)
  $('.visibleFilter img').toggleClass('rotate')
})

// Filtering
$('#storiesfilter').submit(function(){
    $('.filterStories .form-wrap').slideToggle(600)
    $('.filterStories .visibleFilter img').toggleClass('rotate')
    $('.all-stories').fadeOut(300)
    $('.filtered-stories .singleblog').fadeOut(300)
    var filter = $('#storiesfilter')
    $.ajax({
        url:filter.attr('action'),
        data:filter.serialize(), // form data
        type:filter.attr('method'), // POST
        beforeSend:function(xhr){
            // filter.find('button').text('Applying...');
          },
        success:function(data){
            filter.find('button').text('Apply')
            let parsed = JSON.parse(data)

            for (let i = 0; i < parsed.length; i++) {
              let singleStory =
              `
              <a href="${parsed[i].link}" class="singlestory">
                <div class="opaque"></div>
                <div class="img" style="background-image: url('${parsed[i].thumbnail}')"></div>
                <p class="date">${parsed[i].job_title}</p> <!-- date -->
                <h2>${parsed[i].title}</h2> <!-- title -->
                <p class="content">${parsed[i].summary}</p>
                <div class="greyishbox"></div>
                <div class="readmore">
                  <h3>Read Story</h3>
                  <div class="underline"></div>
                </div>
              </a href="${parsed[i].link}">
              `
              $('#filtered-stories').append(singleStory)
            }

            // console.log(data)
            // $('#response').html(arr)
        }
    });
    return false;
});

// ---------- Podcasts PAGE ----------- //
$('.filterPods .form-wrap').hide()
$('.visibleFilter').click(function() {
  $('.filterPods .form-wrap').slideToggle(600)
  $('.visibleFilter img').toggleClass('rotate')
})

// Filtering
$('#podcastfilter').submit(function(){
    $('.filterPods .form-wrap').slideToggle(600)
    $('.filterPods .visibleFilter img').toggleClass('rotate')
    $('.all-pods').fadeOut(300)
    $('.filtered-pods .singlepod').fadeOut(300)
    var filter = $('#podcastfilter')
    $.ajax({
        url:filter.attr('action'),
        data:filter.serialize(), // form data
        type:filter.attr('method'), // POST
        beforeSend:function(xhr){
            // filter.find('button').text('Applying...');
          },
        success:function(data){
            filter.find('button').text('Apply')
            let parsed = JSON.parse(data)

            for (let i = 0; i < parsed.length; i++) {
              let singleStory =
              `
              <a href="${parsed[i].link}" class="singlepod">
                <div class="opaque"></div>
                <div class="img" style="background-image: url('${parsed[i].thumbnail}')"></div>
                <p class="date">Episode ${parsed[i].episode_number}</p> <!-- date -->
                <h2>${parsed[i].title}</h2> <!-- title -->
                <p class="content">${parsed[i].summary}</p>
                <div class="greyishbox"></div>
                <div class="readmore">
                  <h3>Listen to Episode</h3>
                  <div class="underline"></div>
                </div>
              </a href="${parsed[i].link}">
              `
              $('#filtered-pods').append(singleStory)
            }

        }
    });
    return false;
});

// single blog!


// ------ Services PAGE ------ //
if ( $('body').is('.single-post') ) {

  let moreSlider = new Swiper('.more-slider', {
    slidesPerView: 'auto',
    spaceBetween: 10

    // Navigation arrows
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // }

  })



}






});
