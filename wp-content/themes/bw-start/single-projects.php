<?php

/** Template Name: Single Project
 *  Description: The template for the Single Project page
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();



    // renders page
    Timber::render('single-project.twig', $context);
