<?php

/** Template Name: Home
 *  Description: Home Template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();



    // renders page
    Timber::render('page-home.twig', $context);
