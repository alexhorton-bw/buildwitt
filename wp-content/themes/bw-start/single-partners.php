<?php

/** Template Name: Single Partners
 *  Description: The template for the Single Partners page
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();

    // gets the most recent posts
    $partners = array(
      'post_type' => 'partners'
    );

    $context['partners'] = new Timber\PostQuery($partners);



    // renders page
    Timber::render('single-partners.twig', $context);
