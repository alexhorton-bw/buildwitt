<?php

/** Template Name: Partners
 *  Description: Partners Template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();


    // global $paged;
    // if (!isset($paged) || !$paged){
    //     $paged = 1;
    // }

    // gets the most recent posts
    $partners = array(
      'post_type' => 'partners'
      // 'paged' => $paged,
      // 'posts_per_page' => 4
    );

    $context['partners'] = new Timber\PostQuery($partners);

    // renders page
    Timber::render('page-partners.twig', $context);
