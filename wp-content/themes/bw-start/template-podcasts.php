<?php

/** Template Name: Podcasts
 *  Description: Podcasts page template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();




    // pagination
    global $paged;
    if (!isset($paged) || !$paged){
        $paged = 1;
    }


    // gets the most recent posts
    $podcasts = array(
      'post_type' => 'podcasting',
      'paged' => $paged
    );


    $context['terms'] = get_terms('guest');


    $context['podcasts'] = new Timber\PostQuery($podcasts);



    // renders page
    Timber::render('page-podcasts.twig', $context);
