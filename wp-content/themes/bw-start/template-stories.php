<?php

/** Template Name: Stories
 *  Description: Stories page template
 */

    $context = Timber::get_context();

    // gets the WP info from the page (custom fields, title, etc)
    $context['post'] = new TimberPost();




    // pagination
    global $paged;
    if (!isset($paged) || !$paged){
        $paged = 1;
    }


    // gets the most recent posts
    $stories = array(
      'post_type' => 'stories',
      'paged' => $paged,
      'posts_per_page' => 8
    );


    $context['terms'] = get_terms('job_title');


    $context['stories'] = new Timber\PostQuery($stories);



    // renders page
    Timber::render('page-stories.twig', $context);
