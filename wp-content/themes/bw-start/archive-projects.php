<?php
/**
 * The template for displaying Archive Projects pages.
 *
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */


$context = Timber::context();

// gets the projects custom post types
// $projects = array(
// 	'post_type' => 'projects';
// );
//
// $context['projects'] = new Timber\PostQuery($projects);

$context['posts'] = new Timber\PostQuery();

Timber::render('archive-projects.twig', $context);
