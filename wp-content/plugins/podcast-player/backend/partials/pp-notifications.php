<?php
/**
 * Podcast episodes options page template
 *
 * @package Podcast Player
 * @since 1.0.0
 */

?>

<div class="updated notice is-dismissible pp-welcome-notice">
	<p class="intro-msg">
		<?php esc_html_e( 'Thanks for trying/updating Podcast Player.', 'podcast-player' ); ?>
	</p>
	<p><strong style="color: red;"><?php esc_html_e( 'Important: ', 'podcast-player' ); ?></strong><?php esc_html_e( 'If you are using a caching plugin, please clear (purge) the cache to update plugin CSS and JS files.', 'podcast-player' ); ?></p>
	<p>
		<a href="<?php echo esc_url( 'https://vedathemes.com/pp-demo/' ); ?>" target="_blank">
			<?php esc_html_e( 'Check Podcast Player Pro Demo Page', 'podcast-player' ); ?>
		</a>
	</p>
	<h4 style="margin-bottom: 0.25em;padding: 5px;">
		<?php esc_html_e( 'What\'s New in this version?.', 'podcast-player' ); ?>
	</h4>
	<ol>
	<li class="premium">
		<?php esc_html_e( 'We are committed to make Podcast player accessible to visually impaired people. we have audited and fixed few accessibility issues. We will continue to do so in next few updates.', 'podcast-player' ); ?>
	</li>
	<li class="premium">
		<?php esc_html_e( 'Fixed display and styling issues with RTL languages.', 'podcast-player' ); ?>
	</li>
	<li class="premium">
		<?php esc_html_e( 'Fixed display and styling issues with video podcasts. Now, video podcasts should play nicely with Podcast player.', 'podcast-player' ); ?>
	</li>
	<li class="premium">
		<?php esc_html_e( 'Compatibility with Elementor page builder plugin', 'podcast-player' ); ?>
	</li>
	<li class="premium">
		<?php esc_html_e( 'A feature for advanced developers to enter Custom field name in place of feed url for dynamically creating podcast player on different pages.', 'podcast-player' ); ?>
	</li>
	</ol>

	<h4 style="margin-bottom: 0.25em;padding: 5px;">
		<?php esc_html_e( 'What\'s New in Podcast Player Pro.', 'podcast-player' ); ?>
	</h4>
	<ol>
	<li class="premium">
		<?php
		printf(
			'%1$s<a href="%2$s" target="_blank">%3$s</a>.',
			esc_html__( 'Episode search is one of the most prominent method to expose your older episodes and hidden gems. Now, podcast player pro provide more robust and powerful search method. In addition to normal search of free version, it will also check if your user made a minor typo mistake while searching. If it still did not find enough results, it will go more deeper to check if the search term appear within episode content.', 'podcast-player' ),
			'https://vedathemes.com/pp-demo/',
			esc_html__( '(Check Demo)', 'podcast-player' )
		);
		?>
	</li>

	<div class="common-links">
		<p class="pp-link">
			<a href="https://wordpress.org/support/plugin/podcast-player/" target="_blank">
				<?php esc_html_e( 'Raise a support request', 'podcast-player' ); ?>
			</a>
		</p>
		<p class="pp-link">
			<a href="https://wordpress.org/support/plugin/podcast-player/reviews/" target="_blank">
				<?php esc_html_e( 'Give us 5 stars rating', 'podcast-player' ); ?>
			</a>
		</p>
		<p class="pp-link">
			<a href="<?php echo esc_url( wp_nonce_url( add_query_arg( 'pp-dismiss', 'dismiss_admin_notices' ), 'pp-dismiss-' . get_current_user_id() ) ); ?>" target="_parent">
				<?php esc_html_e( 'Dismiss this notice', 'podcast-player' ); ?>
			</a>
		</p>
	</div>
</div>
