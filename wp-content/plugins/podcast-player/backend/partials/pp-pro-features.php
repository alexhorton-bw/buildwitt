<?php
/**
 * Podcast episodes options page template
 *
 * @package Podcast Player
 * @since 1.0.0
 */

?>
<div id="ppot" class="ppot" style="font-size: 14px;">
	<p style="font-size: 14px;margin-top: 0;">
		<?php esc_html_e( 'Podcast player helps you to showcase searchable list of podcast episodes on your website. You can fetch episodes from podcast’s feed url and display them anywhere on your site using widget, shortcode or editor block.', 'podcast-player' ); ?>
		<a href="https://vedathemes.com/blog/vedaitems/podcast-player/">More Info</a>.
	</p>
	<h3>What's New in this version</h3>
	<ol>
		<li><strong>Error Fix: </strong>Accessibility and RTL audit and error fixes</li>
		<li><strong>Error Fix: </strong>Apple subscribe button was not displaying correctly on iPhones.</li>
		<li><strong>Modify: </strong>Elementor edit screen compatibility</li>
		<li><strong>Modify: </strong>Podcast search feature improvements</li>
		<li><strong>Add: </strong>Option to use a custom field name in place of feed url</li>
	</ol>
	<h3>Important Links</h3>
	<ol>
		<li><a href="https://vedathemes.com/blog/vedaitems/podcast-player-pro/" target="_blank">Know more about Podcast Player</a></li>
		<li><a href="https://vedathemes.com/pp-demo/" target="_blank">Pro Version Demo</a></li>
		<li><a href="https://vedathemes.com/documentation/podcast-player/" target="_blank">Free Version Documentation</a></li>
		<li><a href="https://vedathemes.com/documentation/podcast-player-pro/" target="_blank">Pro Version Documentation</a></li>
		<li><a href="https://wordpress.org/support/plugin/podcast-player/" target="_blank">Support Forumn</a></li>
	</ol>
</div>
