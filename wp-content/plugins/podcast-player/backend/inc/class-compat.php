<?php
/**
 * Compatibility with pp pro v 1.0.0.
 *
 * @link       https://www.vedathemes.com
 * @since      1.0.0
 *
 * @package    Podcast_Player
 */

namespace Podcast_Player;

/**
 * Compatibility with pp pro v 1.0.0.
 *
 * @package    Podcast_Player
 * @author     vedathemes <contact@vedathemes.com>
 */
class Compat {

	/**
	 * Holds the instance of this class.
	 *
	 * @since  1.0.0
	 * @access protected
	 * @var    object
	 */
	protected static $instance = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {}

	/**
	 * Register hooked functions.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		// Premium version does not work with legacy mode.
		$legacy = get_option( 'pp-legacy-player' );
		if ( 'on' === $legacy ) {
			return;
		}

		$compat = '1.5.0';

		if ( defined( 'PP_PRO_VERSION' ) && version_compare( PP_PRO_VERSION, $compat, '<' ) ) {
			add_action( 'wp_enqueue_scripts', [ self::get_instance(), 'enqueue_styles' ], 14 );
		}
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * Register public facing stylesheets.
		 */
		wp_enqueue_style(
			'ppcompat',
			plugin_dir_url( __FILE__ ) . 'compat/podcast-player-compat.css',
			[],
			PODCAST_PLAYER_VERSION,
			'all'
		);
	}

	/**
	 * Returns the instance of this class.
	 *
	 * @since  1.0.0
	 *
	 * @return object Instance of this class.
	 */
	public static function get_instance() {

		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}

Compat::init();
