<?php
/**
 * Podcast player display class.
 *
 * @link       https://www.vedathemes.com
 * @since      1.0.0
 *
 * @package    Podcast_Player
 * @subpackage Podcast_Player/public
 */

namespace Podcast_Player;

/**
 * Display podcast player instance.
 *
 * @package    Podcast_Player
 * @subpackage Podcast_Player/public
 * @author     vedathemes <contact@vedathemes.com>
 */
class Display {

	/**
	 * Holds all display styles supported items.
	 *
	 * @since  1.0.0
	 * @access protected
	 * @var array
	 */
	protected $style_supported = [];

	/**
	 * Display init method.
	 *
	 * @param array $props podcast player display props.
	 *
	 * @since  1.0.0
	 */
	protected function render( $props ) {
		if ( ! $props['max'] ) {
			printf( '<div class="error-no-items">%s</div>', esc_html__( 'No Items Found. Try again later.', 'podcast-player' ) );
			return;
		}

		$args = $props['sets'];
		$amsg = '';
		if ( isset( $args['audio-msg'] ) && $args['audio-msg'] ) {
			$type = wp_check_filetype( $args['audio-msg'], wp_get_mime_types() );
			if ( in_array( strtolower( $type['ext'] ), wp_get_audio_extensions(), true ) ) {
				$amsg = $args['audio-msg'];
			}
		}

		$fprint = false;
		if ( 'feed' === $args['fetch-method'] && ( isset( $args['url'] ) && $args['url'] ) ) {
			$fprint = md5( $args['url'] );
		}

		$is_header_available = $this->is_header_available( $args );
		$subscribe_menu      = $this->get_subscribe_menu( $props );
		$this->generate_inline_css( $props );
		$wrapper_class = $this->get_wrapper_classes( $props, $fprint );
		include PODCAST_PLAYER_DIR . 'frontend/partials/podcast-player-public-display.php';
	}

	/**
	 * Display Podcast wrapper classes.
	 *
	 * @param array $props podcast player display props.
	 * @param str   $key   Podcast player unique key.
	 *
	 * @since  1.0.0
	 */
	protected function get_wrapper_classes( $props, $key ) {
		$args          = $props['sets'];
		$wrapper_class = [ 'pp-podcast' ];
		if ( 1 === $props['max'] ) {
			$wrapper_class[] = 'single-episode';
		}

		if ( $this->is_header_available( $args ) ) {
			$wrapper_class[] = 'has-header';
			if ( $this->is_header_visible( $args ) ) {
				$wrapper_class[] = 'header-visible';
			} else {
				$wrapper_class[] = 'header-hidden';
			}
		} else {
			$wrapper_class[] = 'no-header';
		}

		if ( ! $args['hide-featured'] ) {
			$wrapper_class[] = 'has-featured';
		}

		if ( '' === $args['display-style'] || 'legacy' === $args['display-style'] ) {
			$wrapper_class[] = 'playerview';
			if ( $args['list-default'] ) {
				$wrapper_class[] = 'list-default';
			}
		}

		if ( $args['display-style'] ) {
			$wrapper_class[] = $args['display-style'];
			$wrapper_class[] = 'special-style';
		}

		if ( isset( $props['items'][0] ) && isset( $props['items'][0]['mediatype'] ) && $props['items'][0]['mediatype'] ) {
			$wrapper_class[] = 'media-' . $props['items'][0]['mediatype'];
		}

		if ( $args['hide-download'] && $args['hide-social'] ) {
			$wrapper_class[] = 'hide-share';
		} else {
			if ( $args['hide-download'] ) {
				$wrapper_class[] = 'hide-download';
			}
			if ( $args['hide-social'] ) {
				$wrapper_class[] = 'hide-social';
			}
		}

		if ( $this->is_style_support( $args['display-style'], 'bgcolor' ) ) {
			if ( isset( $args['bgcolor'] ) && $args['bgcolor'] ) {
				$bgcolor = podcast_player_hex_to_rgb( $args['bgcolor'], false );
				if ( $bgcolor && ! empty( $bgcolor ) ) {
					$blackcontrast = podcast_player_lumdiff( (int) $bgcolor['red'], (int) $bgcolor['green'], (int) $bgcolor['blue'], 96, 96, 96 );
					$whitecontrast = podcast_player_lumdiff( (int) $bgcolor['red'], (int) $bgcolor['green'], (int) $bgcolor['blue'], 247, 247, 247 );

					if ( $whitecontrast > $blackcontrast ) {
						$wrapper_class[] = 'light-color';
					}
				}
			}
		}

		if ( isset( $args['accent-color'] ) && $args['accent-color'] ) {
			$accentcolor = podcast_player_hex_to_rgb( $args['accent-color'], false );
			if ( $accentcolor && ! empty( $accentcolor ) ) {
				$blackcontrast = podcast_player_lumdiff( (int) $accentcolor['red'], (int) $accentcolor['green'], (int) $accentcolor['blue'], 51, 51, 51 );
				$whitecontrast = podcast_player_lumdiff( (int) $accentcolor['red'], (int) $accentcolor['green'], (int) $accentcolor['blue'], 255, 255, 255 );

				if ( $whitecontrast < $blackcontrast ) {
					$wrapper_class[] = 'light-accent';
				}
			}
		}

		if ( $this->is_style_support( $args['display-style'], 'txtcolor' ) ) {
			if ( isset( $args['txtcolor'] ) && $args['txtcolor'] ) {
				$wrapper_class[] = 'light-text';
			}
		}

		$wrapper_class = apply_filters( 'podcast_player_wrapper_classes', $wrapper_class, $args, $key );
		$wrapper_class = array_map( 'esc_attr', $wrapper_class );
		return join( ' ', $wrapper_class );
	}

	/**
	 * Get podcast subscription menu markup.
	 *
	 * @param array $props podcast player display props.
	 *
	 * @since  1.0.0
	 */
	protected function get_subscribe_menu( $props ) {
		$args = $props['sets'];
		$subs = $args['apple-sub'] || $args['google-sub'];
		if ( ! isset( $subs ) || ! $subs ) {
			return '';
		}

		$apple = '';
		if ( isset( $args['apple-sub'] ) && $args['apple-sub'] ) {
			$apple = sprintf(
				'<a href="%1$s" class="subscribe-item" target="_blank">%2$s<span class="sub-text"><span class="sub-listen-text">%3$s</span><span class="sub-item-text">%4$s</span></span></a>',
				esc_url( $args['apple-sub'] ),
				podcast_player_get_icon( [ 'icon' => 'pp-apple' ] ),
				esc_html__( 'Listen On', 'podcast-player' ),
				esc_html__( 'Apple Podcasts', 'podcast-player' )
			);
		}

		$google = '';
		if ( isset( $args['google-sub'] ) && $args['google-sub'] ) {
			$google = sprintf(
				'<a href="%1$s" class="subscribe-item" target="_blank">%2$s<span class="sub-text"><span class="sub-listen-text">%3$s</span><span class="sub-item-text">%4$s</span></span></a>',
				esc_url( $args['google-sub'] ),
				podcast_player_get_icon( [ 'icon' => 'pp-google' ] ),
				esc_html__( 'Listen On', 'podcast-player' ),
				esc_html__( 'Google Podcasts', 'podcast-player' )
			);
		}

		$subscribe_menu = $apple . $google;
		if ( $subscribe_menu ) {
			if ( $props['nav'] && ! $args['hide-subscribe'] ) {
				$mopen           = sprintf(
					'<button aria-expanded="false" class="pod-items__menu-open pod-button" ><span class="ppjs__offscreen">%1$s</span><span class="btn-icon-wrap">%2$s</span></button>',
					esc_html__( 'Show Menu', 'podcast-player' ),
					podcast_player_get_icon( [ 'icon' => 'pp-dots' ] )
				); // WPCS xss ok.
				$subscribe_menu .= sprintf( '<div class="pod-items__menu">%s%s</div>', $mopen, $props['nav'] );
			}
			$subscribe_menu = sprintf( '<div class="pod-items__navi-menu">%s</div>', $subscribe_menu );
		}

		return $subscribe_menu;
	}

	/**
	 * Check if podcast player header is displayed by default.
	 *
	 * @param array $args Settings for current podcast player instance.
	 *
	 * @since  1.0.0
	 */
	protected function is_header_available( $args ) {
		if ( $args['hide-header'] ) {
			return false;
		}

		if ( $args['hide-cover-img'] && $args['hide-title'] && $args['hide-subscribe'] && $args['hide-description'] ) {
			return false;
		}

		return true;
	}

	/**
	 * Check if podcast player header is displayed by default.
	 *
	 * @param array $args Settings for current podcast player instance.
	 *
	 * @since  1.0.0
	 */
	protected function is_header_visible( $args ) {
		if ( ! $this->is_header_available( $args ) ) {
			return false;
		}

		if ( ( '' === $args['display-style'] || 'legacy' === $args['display-style'] ) && ! $args['header-default'] ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Get thumbnail image sizes attribute.
	 *
	 * @param array $args Settings for current Podcast player instance.
	 *
	 * @since  1.0.0
	 */
	protected function get_thumb_image_sizes( $args ) {
		$style  = $args['display-style'];
		$colnum = $args['grid-columns'];

		if ( 'lv2' === $style ) {
			return '60px';
		} elseif ( 'lv1' === $style ) {
			return '(max-width: 640px) 100vw, 640px';
		} elseif ( 'gv1' === $style ) {
			$sizes = '(max-width: 640px) 100vw, (max-width: 768px) 60vw';
			if ( 2 === $colnum ) {
				$sizes .= ', 60vw';
			}
			if ( 2 < $colnum ) {
				$sizes .= '(max-width: 1024px) 40vw';
			}
			if ( 3 === $colnum ) {
				$sizes .= ', 40vw';
			}
			if ( 3 < $colnum ) {
				$width  = ( ( 100 / $colnum ) + 10 ) . 'vw';
				$sizes .= ', ' . $width;
			}
			return $sizes;
		} else {
			return '';
		}
	}

	/**
	 * Display Podcast episode search field.
	 *
	 * @since  1.0.0
	 */
	protected function search_field() {
		?>
		<div class="episode-list__search">
			<label class="label-episode-search"><span class="ppjs__offscreen"><?php esc_attr_e( 'Search Episodes', 'podcast-player' ); ?></span><input type="text" placeholder="<?php esc_attr_e( 'Search Episodes', 'podcast-player' ); ?>" title="<?php esc_attr_e( 'Search Podcast Episodes', 'podcast-player' ); ?>"/></label><span class="episode-list__search-icon"><?php echo podcast_player_get_icon( [ 'icon' => 'pp-search' ] );// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></span><button class="episode-list__clear-search pod-button"><?php echo podcast_player_get_icon( [ 'icon' => 'pp-x' ] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?><span class="ppjs__offscreen"><?php esc_html_e( 'Clear Search Field', 'podcast-player' ); ?></span></button>
		</div>
		<?php
	}

	/**
	 * Display Podcast inline css.
	 *
	 * @param array $props podcast player display props.
	 *
	 * @since  1.0.0
	 */
	protected function generate_inline_css( $props ) {
		$css = '';
		$id  = '#pp-podcast-' . absint( $props['inst'] );
		$mod = '.modal-' . absint( $props['inst'] );
		if ( isset( $props['sets']['accent-color'] ) && $props['sets']['accent-color'] ) {
			$color = sanitize_hex_color( $props['sets']['accent-color'] );
			$rgb   = podcast_player_hex_to_rgb( $color, true );
			$css  .= sprintf(
				'
				%1$s a,
				%4$s a,
				%1$s .ppjs__more {
					color: %2$s;
				}
				%1$s.postview .episode-list__load-more {
					color: %2$s !important;
				}
				%1$s.postview .episode-list__load-more,
				%1$s .ppjs__time-handle-content,
				%4$s .ppjs__time-handle-content {
					border-color: %2$s !important;
				}
				%1$s .ppjs__audio .ppjs__button.ppjs__playpause-button button,
				%1$s button.episode-list__load-more,
				%1$s .ppjs__audio-time-rail,
				%1$s.lv3 .pod-entry__play,
				%4$s .ppjs__audio .ppjs__button.ppjs__playpause-button button,
				%4$s button.episode-list__load-more,
				%4$s .ppjs__audio-time-rail,
				%4$s button.pp-modal-close,
				%1$s .pod-entry.activeEpisode:before,
				%4$s .pod-entry.activeEpisode:before {
					background-color: %2$s !important;
				}
				%1$s .hasCover .ppjs__audio .ppjs__button.ppjs__playpause-button button {
					background-color: rgba(0, 0, 0, 0.5) !important;
				}
				%1$s button.episode-list__load-more:hover,
				%1$s button.episode-list__load-more:focus,
				%4$s button.episode-list__load-more:hover,
				%4$s button.episode-list__load-more:focus {
					background-color: rgba( %3$s, 0.8 ) !important;
				}
				%1$s .ppjs__button.toggled-on,
				%4$s .ppjs__button.toggled-on {
					background-color: rgba( %3$s, 0.1 );
				}
				%1$s.postview .episode-list__load-more:hover,
				%1$s.postview .episode-list__load-more:focus {
					background-color: rgba( %3$s, 0.1 ) !important;
				}
				%1$s.postview .episode-list__load-more {
					background-color: transparent !important;
				}
				',
				$id,
				$color,
				$rgb,
				$mod
			);
		}

		if ( $this->is_style_support( $props['sets']['display-style'], 'bgcolor' ) ) {
			if ( isset( $props['sets']['bgcolor'] ) && $props['sets']['bgcolor'] ) {
				$css .= sprintf(
					'
					%1$s.playerview,
					%1$s.playerview .episode-single,
					.inline-view %3$s {
						background-color: %2$s!important;
					}
					',
					$id,
					sanitize_hex_color( $props['sets']['bgcolor'] ),
					$mod
				);
			}
		}

		if ( $props['sets']['hide-download'] && $props['sets']['hide-social'] ) {
			$css .= sprintf(
				'
				%1$s .ppjs__share-button,
				%2$s .ppjs__share-button {
					display: none;
				}
				',
				$id,
				$mod
			);
		}

		if ( $props['sets']['hide-content'] ) {
			$css .= sprintf(
				'
				%1$s .ppjs__script-button {
					display: none;
				}
				',
				$id
			);
		}

		if ( $props['sets']['hide-author'] ) {
			$css .= sprintf(
				'
				%1$s .pod-entry__author {
					display: none;
				}
				',
				$id
			);
		}

		if ( $props['sets']['header-default'] ) {
			$css .= sprintf(
				'
				%1$s .pod-info__header {
					display: block;
				}
				',
				$id
			);
		}

		if ( isset( $props['sets']['font-family'] ) && $props['sets']['font-family'] ) {
			$ff     = $props['sets']['font-family'];
			$fflist = apply_filters( 'podcast_player_fonts', [] );
			if ( isset( $fflist[ $ff ] ) ) {
				$font = $fflist[ $ff ];
				$css .= sprintf(
					'
					%1$s,
					%1$s input,
					%1$s .episode-list__load-more,
					%2$s {
						font-family: "%3$s",-apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol !important;
					}
					',
					$id,
					$mod,
					$font
				);
			}
		}
		?>
		<style type="text/css"><?php echo wp_strip_all_tags( $css, true ); ?></style>
		<?php
	}

	/**
	 * Get elements supported by selected style.
	 *
	 * @return array
	 */
	public function get_style_supported() {
		if ( ! empty( $this->style_supported ) ) {
			return $this->style_supported;
		}

		$styles = podcast_player_default_styles();
		foreach ( $styles as $style => $args ) {
			$this->style_supported[ $style ] = $args['support'];
		}

		return $this->style_supported;
	}

	/**
	 * Check if item is supported by the style.
	 *
	 * @param string $style Current display style.
	 * @param string $item  item to be checked for support.
	 * @return bool
	 */
	public function is_style_support( $style, $item ) {
		$supported = $this->get_style_supported();

		if ( ! isset( $supported[ $style ] ) ) {
			return false;
		}
		return in_array( $item, $supported[ $style ], true );
	}

	/**
	 * Display Podcast head info toggle button.
	 *
	 * @since  1.0.0
	 */
	protected function header_info_toggle() {
		printf(
			'<button aria-expanded="false" class="pod-info__toggle"><span class="ppjs__offscreen">%1$s</span>%2$s%3$s</button>',
			esc_html__( 'Show Podcast Details', 'podcast-player' ),
			podcast_player_get_icon( [ 'icon' => 'pp-podcast' ] ), // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			podcast_player_get_icon( [ 'icon' => 'pp-x' ] ) // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		);
	}
}
